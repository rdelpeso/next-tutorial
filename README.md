# next-tutorial

This is a sample repository with the steps for setting up a modular Next.JS.

## Branches and their features

Basics

```bash
npm init -y
npm install -E next react react-dom
mkdir pages
echo 'export default () => <h1>Hello</h1>' > pages/index.js
cat << EOF > .gitignore
dist
node_modules
*.log
.next
data
mysql-root
EOF
npx next
```


Typing it up!

```bash
npm install -ED @zeit/next-typescript typescript tslint @types/next @types/react @types/react-dom
mv pages/index.js pages/index.ts
echo 'module.exports = require("@zeit/next-typescript")()' > next.config.js
cat << EOF > tsconfig.json
{
  "compileOnSave": false,
  "compilerOptions": {
    "target": "esnext",
    "module": "esnext",
    "jsx": "preserve",
    "allowJs": true,
    "moduleResolution": "node",
    "allowSyntheticDefaultImports": true,
    "noUnusedLocals": true,
    "noUnusedParameters": true,
    "removeComments": false,
    "preserveConstEnums": true,
    "sourceMap": true,
    "skipLibCheck": true,
    "baseUrl": ".",
    "typeRoots": [
      "./node_modules/@types"
    ],
    "lib": [
      "dom",
      "es2015",
      "es2016"
    ]
  },
  "exclude": ["next.config.js"]
}
EOF
cat << EOF > .babelrc
{
  "presets": [
    "next/babel",
    "@zeit/next-typescript/babel"
  ]
}
EOF
npx next
```


Lipstick on the pig?

```bash
npm install -ED bootstrap @types/bootstrap @zeit/next-css
echo 'module.exports = require("@zeit/next-css")(require("@zeit/next-typescript")())' > next.config.js
rm pages/index.ts
cat << EOF > pages/index.tsx
import 'bootstrap/dist/css/bootstrap.css'

export default () =>
  <div className="container">
    <h1>Title</h1>
    <form>
      <legend>Form</legend>
      <div className="form-row">
        <div className="form-group col-md-6">
          <input type="text" className="form-control" placeholder="Field" />
        </div>
        <div className="form-group col-md-6">
          <input type="text" className="form-control" placeholder="Field" />
        </div>
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  </div>
EOF
npx next
```


Fetch!

```bash
npm install -E isomorphic-unfetch
cat << EOF > pages/index.tsx
import fetch from 'isomorphic-unfetch'

import 'bootstrap/dist/css/bootstrap.css'

const Page = ({img_src}) =>
  <div className="container">
    <h1>Title</h1>
    <form>
      <legend>Form</legend>
      <div className="form-row">
        <div className="form-group col-md-6">
          <input type="text" className="form-control" placeholder="Field" />
        </div>
        <div className="form-group col-md-6">
          <input type="text" className="form-control" placeholder="Field" />
        </div>
      </div>
      <div className="form-row">
        <div className="form-group col-md-12">
          <img src={img_src} />
        </div>
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  </div>

Page.getInitialProps = async ({req}) => {
  const res = await fetch('https://dog.ceo/api/breeds/image/random')
  const json = await res.json()
  return { img_src: json.message }
}

export default Page
EOF
npx next
```


So you think you can serve?

```bash
npm install -E express ts-optchain
npm install -ED @types/express nodemon ts-node
mkdir server
cat << EOF > server/index.ts
import * as express from 'express'
import * as next from 'next'
import { oc } from 'ts-optchain'

import API from './api'

const app = express()
const server = require('http').Server(app)

const port = parseInt(oc(process).env.PORT('3000'), 10)
const dev = process.env.NODE_ENV !== 'production'
const nextApp = next({ dev })
const handle = nextApp.getRequestHandler()

nextApp.prepare().then(() => {
  app
    .use('/api', API)
    .all('*', (req, res) => handle(req, res))
  server
    .listen(port, (err: any) => {
      if (err) {
        throw err
      }
      console.log(\`> Ready on port \${port}\`)
    })
})
EOF
cat << EOF > server/api.ts
import { Router } from 'express'

const router = Router()
router.get('/status', (req, res) =>
  res.json({
    url: req.baseUrl,
    version: '1.0.0',
  })
)

export default router
EOF
cat << EOF > nodemon.json
{
  "watch": ["server/**/*.ts"],
  "execMap": {
    "ts": "ts-node --typeCheck --compilerOptions '{\"module\":\"commonjs\"}'"
  }
}
EOF
cat << EOF > tsconfig.server.json
{
  "extends": "./tsconfig.json",
  "compilerOptions": {
    "module": "commonjs",
    "outDir": ".next/production-server/"
  },
  "include": ["server/**/*.ts"]
}
EOF
npx nodemon server/index.ts
```


Bonus round.

Package all the things:

```bash
cat << EOF > package.bash
npm install
rm -fr .next
NODE_ENV=production npx next build
NODE_ENV=production npx tsc --project tsconfig.server.json
rm -fr node_modules
NODE_ENV=production npm install
rm -fr dist
mkdir dist
cp -a .next node_modules dist
EOF
chmod +x package.bash
```

Run over that package!

```bash
cat << EOF > run.bash
cd dist
NODE_ENV=production node .next/production-server/index.js
EOF
chmod +x run.bash
```

And that's it. You got a Next.JS app, running in TypeScript, with Bootstrap CSS, and an ExpressJS API.

Happy procrastinating!

import * as express from 'express'
import * as next from 'next'
import { oc } from 'ts-optchain'

import API from './api'

const app = express()
const server = require('http').Server(app)

const port = parseInt(oc(process).env.PORT('3000'), 10)
const dev = process.env.NODE_ENV !== 'production'
const nextApp = next({ dev })
const handle = nextApp.getRequestHandler()

nextApp.prepare().then(() => {
  app
    .use('/api', API)
    .all('*', (req, res) => handle(req, res))
  server
    .listen(port, (err: any) => {
      if (err) {
        throw err
      }
      console.log(`> Ready on port ${port}`)
    })
})

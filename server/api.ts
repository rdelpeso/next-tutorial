import { Router } from 'express'

const router = Router()
router.get('/status', (req, res) =>
  res.json({
    url: req.baseUrl,
    version: '1.0.0',
  })
)

export default router

import fetch from 'isomorphic-unfetch'

import 'bootstrap/dist/css/bootstrap.css'

const Page = ({img_src}) =>
  <div className="container">
    <h1>Title</h1>
    <form>
      <legend>Form</legend>
      <div className="form-row">
        <div className="form-group col-md-6">
          <input type="text" className="form-control" placeholder="Field" />
        </div>
        <div className="form-group col-md-6">
          <input type="text" className="form-control" placeholder="Field" />
        </div>
      </div>
      <div className="form-row">
        <div className="form-group col-md-12">
          <img src={img_src} />
        </div>
      </div>
      <button type="submit" className="btn btn-primary">Submit</button>
    </form>
  </div>

Page.getInitialProps = async ({req}) => {
  const res = await fetch('https://dog.ceo/api/breeds/image/random')
  const json = await res.json()
  return { img_src: json.message }
}

export default Page
